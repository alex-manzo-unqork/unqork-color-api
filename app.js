const express = require("express");
const app = express();
const cors = require("cors");
const port = process.env.PORT || 3000;

const chroma = require("chroma-js");

app.use(cors());

app.listen(port, () => {
  console.log(`server started at port ${port}`);
});

app.get("/", (req, res) => {
  try {
    const { colorName, colorValue } = req.query;
    const colorObj = generateColorShades(colorName, colorValue);
    const cssString = generateCssString(colorObj);
    res.send(JSON.stringify(cssString));
  } catch (e) {
    res.send("Something happened.");
  }
});

app.get("/contrast", (req, res) => {
  try {
    const { textColor, bgColor } = req.query;
    const contrastScore = checkContrast(textColor, bgColor);
    res.send(JSON.stringify(contrastScore));
  } catch (e) {
    res.send("Something happened.");
  }
});

const checkContrast = (textColor, bgColor) => {
  return chroma.contrast(textColor, bgColor);
}

const generateColorShades = (colorName, colorValue) => {
  const lightScale = chroma.scale([colorValue, "white"]);
  const darkScale = chroma.scale([colorValue, "black"]);

  const colorObj = {};
  colorObj[`--unq-color-${colorName}`] = chroma(colorValue).css('hsl');
  colorObj[`--unq-color-${colorName}-${50}`] = lightScale(0.9).css('hsl');
  colorObj[`--unq-color-${colorName}-${100}`] = lightScale(0.75).css('hsl');
  colorObj[`--unq-color-${colorName}-${200}`] = lightScale(0.6).css('hsl');
  colorObj[`--unq-color-${colorName}-${300}`] = lightScale(0.4).css('hsl');
  colorObj[`--unq-color-${colorName}-${400}`] = lightScale(0.25).css('hsl');
  colorObj[`--unq-color-${colorName}-${500}`] = chroma(colorValue).css('hsl');
  colorObj[`--unq-color-${colorName}-${600}`] = darkScale(0.25).css('hsl');
  colorObj[`--unq-color-${colorName}-${700}`] = darkScale(0.4).css('hsl');
  colorObj[`--unq-color-${colorName}-${800}`] = darkScale(0.55).css('hsl');
  colorObj[`--unq-color-${colorName}-${900}`] = darkScale(0.7).css('hsl');

  // Alpha
  const opacityStops = [
    0.05, 0.1, 0.2, 0.25, 0.3, 0.4, 0.5, 0.6, 0.7, 0.75, 0.8, 0.9,
  ];

  opacityStops.forEach(
    (alpha) =>
      (colorObj[`--unq-color-${colorName}-a${alpha * 100}`] = chroma(colorValue)
        .alpha(alpha)
        .css('hsl'))
  );

  return colorObj;
};

const generateCssString = (obj) => {
  const mappedValues = Object.entries(obj)
    .map(([key, value]) => {
      return `  ${key}: ${value};\n`;
    })
    .join("");

  return `:root {\n${mappedValues}}\n`;
};
