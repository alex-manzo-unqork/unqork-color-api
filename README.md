# Unqork Color API

## Usage
Send a request with `colorName` and `colorValue` as query parameters to receive back a CSS String with shades and alpha variants of a given color.
### Parameters
#### colorName
String
Expected values: primary, secondary, tertiary, info, success, danger, light, dark

#### colorValue
String
Can be a color name like blue, red, green, purple or a HEX code. Will work with or without `#`.
### Example
Request: `?colorName=primary&colorValue=33768e`
Response:
```json
":root {\n  --unq-color-secondary: hsl(195.82,47.15%,37.84%);\n  --unq-color-secondary-50: hsl(195.82,28.71%,93.78%);\n  --unq-color-secondary-100: hsl(195.82,28.71%,84.46%);\n  --unq-color-secondary-200: hsl(195.82,28.71%,75.14%);\n  --unq-color-secondary-300: hsl(195.82,28.71%,62.71%);\n  --unq-color-secondary-400: hsl(195.82,28.71%,53.38%);\n  --unq-color-secondary-500: hsl(195.82,47.15%,37.84%);\n  --unq-color-secondary-600: hsl(195.82,47.15%,28.38%);\n  --unq-color-secondary-700: hsl(195.82,47.15%,22.71%);\n  --unq-color-secondary-800: hsl(195.82,47.15%,17.03%);\n  --unq-color-secondary-900: hsl(195.82,47.15%,11.35%);\n  --unq-color-secondary-a5: hsla(195.82,47.15%,37.84%,0.05);\n  --unq-color-secondary-a10: hsla(195.82,47.15%,37.84%,0.1);\n  --unq-color-secondary-a20: hsla(195.82,47.15%,37.84%,0.2);\n  --unq-color-secondary-a25: hsla(195.82,47.15%,37.84%,0.25);\n  --unq-color-secondary-a30: hsla(195.82,47.15%,37.84%,0.3);\n  --unq-color-secondary-a40: hsla(195.82,47.15%,37.84%,0.4);\n  --unq-color-secondary-a50: hsla(195.82,47.15%,37.84%,0.5);\n  --unq-color-secondary-a60: hsla(195.82,47.15%,37.84%,0.6);\n  --unq-color-secondary-a70: hsla(195.82,47.15%,37.84%,0.7);\n  --unq-color-secondary-a75: hsla(195.82,47.15%,37.84%,0.75);\n  --unq-color-secondary-a80: hsla(195.82,47.15%,37.84%,0.8);\n  --unq-color-secondary-a90: hsla(195.82,47.15%,37.84%,0.9);\n}\n"
```